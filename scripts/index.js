var apiBuilder = angular.module("apiBuilder", []);

apiBuilder.filter('getSummary', function() {
    return function(outputTask) {
        console.log(`Filtering ${outputTask}`);
        console.log(`trying to return ${outputTask.taskId}`);
        return outputTask.summary;
    }
});

apiBuilder.controller("Ctrl",

function Ctrl($scope, $http) {

    $http.get(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/customApi/getAllKeys')
        .then(function(response) {
            console.log(JSON.stringify(response.data));
            $scope.uriList = response.data;
            $scope.selected = {};
        });

    $http.get(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/workflow_builder/workflows/list')
        .then(function(response) {
            console.log(response.data)
            $scope.wfList = response.data;
            setTaskList("DBTest");
        });

    // gets the template to ng-include for a table row / item
    $scope.getTemplate = function (uri) {
        if (uri._id === $scope.selected._id) return 'edit';
        else return 'display';
    };

    $scope.editUri = function (uri) {
        $scope.selected = angular.copy(uri);
        setTaskList(uri.workflow);
    };

    $scope.saveUri = function (idx) {
        console.log(`Saving key at index ${idx}/${$scope.uriList.length-1}: ${JSON.stringify($scope.uriList[idx])}`);
        /*$scope.model.contacts[idx] = angular.copy($scope.model.selected);
        $scope.reset();*/
        $scope.uriList[idx].outputTask = JSON.parse($scope.uriList[idx].outputTask);

        $http.post(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/customApi/saveRunKey',
            $scope.uriList[idx]).then(function(response) {
                console.log(`Successfully saved: ${JSON.stringify(response.data)}`)
                $scope.selected = {};
                $scope.taskListOptions = [];
                if($scope.uriList[idx]._id === -1)
                $scope.uriList[idx]._id = response.data._id;
            });
    };

    $scope.reset = function () {
        $scope.selected = {};
        $scope.taskListOptions = [];
    };

    $scope.newRow = function() {
        console.log($scope.uriList[$scope.uriList.length-1]);
        if($scope.uriList.length === 0 || $scope.uriList[$scope.uriList.length-1].hasOwnProperty('key')){
            $scope.uriList.push({_id:-1});
            $scope.selected._id = -1;
            $scope.taskListOptions = [];
        }
    }

    $scope.deleteUri = function(idx) {
        console.log(`Deleting key at index ${idx}/${$scope.uriList.length-1}: ${JSON.stringify($scope.uriList[idx])}`);

        let uriToDelete = $scope.uriList[idx];

        $http.delete(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/customApi/deleteKey/' + uriToDelete.key)
            .then(function(response) {
                //locate and remove from uriList
                $scope.uriList.splice(idx, 1);
            });
    }

    setTaskList = function(wfName) {
        console.log(`Setting Task list for ${wfName}`);
        //let wfName = $scope.uriList[idx].workflow;
        $http.get(window.location.protocol + '//' + window.location.hostname + ':' + window.location.port + '/customApi/getWFTasks/' + wfName)
            .then(function(response) {
                console.log(`task List Options: ${JSON.stringify(response.data)}`);
                $scope.taskListOptions = response.data;
            });
    }

    $scope.changeWorkflow = function(idx) {
        let wfName = $scope.uriList[idx].workflow;
        setTaskList(wfName);
    }
});