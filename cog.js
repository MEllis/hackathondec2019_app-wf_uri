/* OPTIONS:

    name                    default     description

    waitForCompletion       true        Waits for the job to complete before returning - returns with data. (note: if false, does not wait, nor does it return data)
    maxWaitTime             300000      Timer to wait, defaults to max IAP Wait Timer, can be set shorter
    completionOnly          false       Overrides the resultsTask return and returns true when the Workflow completes, false if the timeout is exceeded

*/
const wait = ms => new Promise(resolve => setTimeout(resolve, ms));

class customApi {

    async getAllKeys(callback) {
        log.debug('Fetching all WF URI Key entries');
        let urikeys = database.collection('wfuri_keys');

        let data = await urikeys.find({}).toArray();
        return callback(data);
    }

    async deleteKey(key, callback) {
        log.debug(`deleting ${key}`);
        let urikeys = database.collection('wfuri_keys');

        await urikeys.deleteOne({"key": key});
        return callback(`Entry for ${key} deleted`);
    }

    async runWorkflow(key, options, inputs, callback) {
        log.debug(`Running WF Key ${key} with options ${JSON.stringify(options)} and WF Inputs ${JSON.stringify(inputs)}`);
        let urikeys = database.collection('wfuri_keys');

        //locate key entry
        let WFKey = await urikeys.findOne({"key": key});        

        if (WFKey === null) {
            return callback(null, "Key not found");
        }
        
        log.debug(`Found WF Key Entry: ${JSON.stringify(WFKey)}`);

        let runOptions = {
            'wait': true,
            'waitTime': 300000,
            'completion': false
        }

        //parse options
        if(options !== null && Object.keys(options).length > 0) {
            runOptions = {
                'wait': options.WaitForJobCompletion === null ? true : options.WaitForJobCompletion,
                'waitTime': options.maxWaitTime || 300000,
                'completion': options.completionOnly || false
            }
        }

        log.debug(`Running with Options: ${JSON.stringify(runOptions)}`);

        let respObj = {
            'status': 'temp',
            'results': 'temp',
            'jobid': 'temp'
        };
        //start job
        cogs.WorkFlowEngine.startJobWithOptions(WFKey.workflow, {'variables': inputs}, async (res, err) => {
            log.debug(`Job Started with ID: ${res._id}`);
            if(err) {
                log.error(err);
                return callback(null, err);
            }

            respObj.jobid = res._id;
            
            //if waiting
                //if timeout reached - return nothing or error??
                //Wait for job to finish
                //get task details for job ID...for the output task
                //return task result
            
            if(runOptions.wait) {
                log.debug(`Waiting on job completion or Timeout`);
                let waitStatus = await this.WaitForJobCompletion(res._id, runOptions.waitTime);
                log.debug(`Finished waiting.  Final status: ${waitStatus}`);
                respObj.status = waitStatus;

                if(waitStatus === "error") {
                    respObj.results = "Job has encountered an error.  Please access IAP Job Manager for more details";
                } else if (waitStatus !== "complete" ) {
                    respObj.results = `Job was unable to complete within the timeout window of ${runOptions.waitTime}ms. Job is still running`;
                } else {//completed
                    //get results from job task
                    respObj.results = await this.getCompletedTaskData(res._id, WFKey.outputTask.taskId);
                }
                log.debug(`Returning: ${JSON.stringify(respObj)}`);
                return callback(respObj);
            }
            
            //return nothing
            return callback("Job Started");
        });
    }

    getTaskIterations(jobId, taskId) {
        return new Promise((res, rej) => {
            cogs.WorkFlowEngine.getTaskIterations(jobId, taskId, (resp, err) => {
                if (err) {
                    return rej(err);
                }
                return res(resp);
            });
        });
    }

    async getCompletedTaskData(jobId, taskId) {
        let taskData = await this.getTaskIterations(jobId, taskId);

        //ASSUMPTION: Task marked as return value will only run once
        return taskData[0].variables.outgoing;
    }

    async saveRunKey(key, workflow, outputTask, callback) {
        try {
            log.debug(`Saving Key: ${key} attached to Workflow: ${workflow} and looking for output from task: ${outputTask}`);
            let urikeys = database.collection('wfuri_keys');

            //??validate Workflow and Task exist??

            //Update path
            if(await urikeys.countDocuments({'key': key}) > 0) {
                log.debug("Already exists, updating");
                await urikeys.updateOne({"key": key}, {"$set": {"workflow": workflow, "outputTask": outputTask}}, {"upsert": false});
            } else {//insert path
                log.debug("new entry, creating");
                await urikeys.insertOne({"key": key, "workflow": workflow, "outputTask": outputTask});
            }

            //return the object FROM The database we just updated.  Do it this way to catch the new ID field (for UI purposes)
            let retVal = await urikeys.findOne({"key": key});
            
            return callback(retVal);
        } catch (err) {
            log.error(err);
            return callback(null, err);
        }
    }

    getWFDetails(workflow) {
        return new Promise((res, rej) => {
            cogs.WorkFlowEngine.getWorkflowsDetailedByName(workflow, (resp, err) => {
                if (err) {
                    return rej(err);
                }
                return res(resp);
            });
        })
    }

    async getWFTasks(workflow, callback) {
        try {
            log.debug(`Getting details for ${workflow}`);
            let wfDetails = await this.getWFDetails(workflow);
            let allTaskSummaryIds = [];
            let taskKeysArray = Object.keys(wfDetails.tasks);
            log.debug(`Task Keys: ${taskKeysArray}`);
            for(let index = 0; index < taskKeysArray.length; index++) {
                let current = taskKeysArray[index];
                if(current === "workflow_start" || current === "workflow_end") {
                    continue;
                }

                //log.debug(`Storing: ${JSON.stringify({"taskId": current, "summary": wfDetails.tasks[current].summary})}`);
                allTaskSummaryIds.push({"taskId": current, "summary": wfDetails.tasks[current].summary || ""});

                if(index === taskKeysArray.length-1) {
                    log.debug(`returning: ${JSON.stringify(allTaskSummaryIds)}`);
                    return callback(allTaskSummaryIds);
                }
            }
        } catch (err) {
            log.error(err);
            return callback(null, err);
        }
    }

    getJobStatus(jobId) {
        return new Promise((res, rej) => {
            cogs.WorkFlowEngine.getJobShallow(jobId, (resp, err) => {
                if(err) {
                    return rej(err);
                }
                return res(resp.status);
            });
        });
    }

    async WaitForJobCompletion(jobId, timeout) {
        //do

        //get job status
        //WFE.getJobByID call

        //while job is running (should stop on Compelte OR on Error)

        //return
        let jobStatus = null;
        let startTime = Date.now();
        do {
            await wait(250);
            jobStatus = await this.getJobStatus(jobId);
            if(jobStatus === "complete" || jobStatus === "error") {
                return jobStatus;
            }
        } while (jobStatus !== "complete" || jobStatus !== "error" || Date.now() - startTime >= timeout)

        return jobStatus;
    }

    
}

module.exports = new customApi();